# rswitcher

A software created to easy switch between HiDPI mode with fractional scaling and regular resolution for Linux Mint.
Its also installed as a applet to have a easy access from the desktop.

# Install
Run install.sh to install it as a applet and as a shellscript. 
The shell-script will install in ~.local/bin and the applet in .local/share/cinnamon/applets

# rswitcher usage

<code>Usage: rswitcher [OPTION]...
Set primary resolution with fractional scaling options
    -r --resolution [ARG]        Resolution of the display
                                 Set [ARG] 'max' to get maximum resolution possible.
  -s --scalingfactor [ARG]     The GTK scaling factor: 1 - 3
  -f --fractionalscaling [ARG] Fractional scaling in percent: 125, 150, 175 200
Example: rswitcher --resolution 3840x2160 --scalingfactor 2 --fractionalscaling 150
Will result in 150% factional scaling with a perceived resolution output at 2560x1440
</code>
